const getEmailDetailsList = require('../problems/problem1');

const dataSet = require('../js_drill_5.js');

try {
    const emailDetailsList = getEmailDetailsList(dataSet);
    console.log(emailDetailsList);
} catch (error) {
    console.error(error.message);
}