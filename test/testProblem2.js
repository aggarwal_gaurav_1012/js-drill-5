const groupProjectsByStatus = require('../problems/problem2');

const dataSet = require('../js_drill_5.js');

try {
    const result = groupProjectsByStatus(dataSet);
    console.log(result);
} catch (error) {
    console.error('Error:', error.message);
}