const groupProjectsByStatus = dataSet => {
    if (!Array.isArray(dataSet)) {
        throw new Error('Input is not an array');
    }

    return dataSet.reduce((result, person) => {
        if (!person.projects || !Array.isArray(person.projects)) {
            throw new Error('Invalid project data for a person');
        }

        person.projects.forEach(project => {
            if (!project.name || !project.status) {
                throw new Error('Invalid project data');
            }

            if (!result[project.status]) {
                result[project.status] = [];
            }

            result[project.status].push(project.name);
        });

        return result;
    }, {});
};

module.exports = groupProjectsByStatus;