const getEmailDetails = (email) => {
    const [namePart, domain] = email.split('@');
    const [firstName, lastName] = namePart.split('.').map(part => part.charAt(0).toUpperCase() + part.slice(1));
    return {
        firstName,
        lastName,
        emailDomain: domain
    };
};

const getEmailDetailsList = (dataSet) => {
    if (!Array.isArray(dataSet)) {
        throw new Error('Input is not an array');
    }

    return dataSet.map((person) => {
        if (!person || !person.email || typeof person.email !== 'string') {
            throw new Error('Invalid email format');
        }
        return getEmailDetails(person.email);
    });
};

module.exports = getEmailDetailsList;  