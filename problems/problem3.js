// Function to filter unique languages from the dataset
function filterUniqueLanguages(dataSet) {
    
    return dataSet.reduce((acc, curr) => {
        // Merge current person's languages with accumulator array
        const mergedLanguages = [...acc, ...curr.languages];

        // Filter out duplicates from the merged array
        return mergedLanguages.filter((language, index) => mergedLanguages.indexOf(language) === index);
    }, []);
}

module.exports = filterUniqueLanguages;  